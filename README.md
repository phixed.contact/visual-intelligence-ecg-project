# Visual Intelligence ECG Project

## Dataset
* A large scale 12-lead electrocardiogram database for arrhythmia study
* [MIT-BIH Arrhythmia Database](https://physionet.org/content/mitdb/1.0.0/)
* Website: https://www.physionet.org/content/ecg-arrhythmia/1.0.0/
* Authors: Jianwei Zheng, Hangyuan Guo, Huimin Chu
