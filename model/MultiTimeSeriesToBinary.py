import torch
import torch.nn as nn


class MultiTimeSeriesToBinary(nn.Module):
    def __init__(self, num_time_series, series_length, batch_size, num_classes=2):
        super(MultiTimeSeriesToBinary, self).__init__()

        self.series_length = series_length
        self.batch_size = batch_size

        # Define the convolutional layers
        self.conv1d_layers = nn.ModuleList([
            nn.Conv1d(in_channels=num_time_series, out_channels=num_time_series*2, kernel_size=3),
            nn.Conv1d(in_channels=num_time_series*2, out_channels=num_time_series*4, kernel_size=3),
            nn.Conv1d(in_channels=num_time_series*4, out_channels=num_time_series*8, kernel_size=3)
        ])

        # Calculate the input size for the fully connected layer
        conv_output_size = self._get_conv_output_size(num_time_series, series_length)

        print("aa conv_output_size: ", conv_output_size)

        # Define the fully connected layers
        self.fc_layers = nn.Sequential(
            nn.Linear(conv_output_size, 128),
            nn.ReLU(),
            # nn.Dropout(0.5),  # TODO: should I use dropout?
            nn.Linear(128, num_classes),
            # no real difference if using softmax or logsoftmax or none, kinda strange
            # nn.Softmax(dim=1)
            nn.LogSoftmax(dim=1)
        )

        # Weights initialization
        for layer in self.fc_layers:
            if isinstance(layer, nn.Linear):
                nn.init.xavier_uniform_(layer.weight)

    def _get_conv_output_size(self, num_time_series, series_length):
        print("_get_conv_output_size")
        # Dummy input to calculate the size after convolutional layers
        x = torch.randn(1, num_time_series, series_length)
        # x = torch.randn(1, num_time_series * series_length)
        print(x.shape)
        x = self._forward_conv_layers(x)
        print(x.view(1, -1).shape)
        print(x.view(1, -1).size(1))
        return x.view(1, -1).size(1)

    def _forward_conv_layers(self, x):
        for conv_layer in self.conv1d_layers:
            x = conv_layer(x)
            x = nn.ReLU()(x)
            x = nn.MaxPool1d(kernel_size=2)(x)
        return x

    def forward(self, x):
        # print("forward: ", x.shape)
        # Reshape input to (batch_size, num_time_series * series_length)
        batch_size = x.size(0)
        # Reshape to (batch_size, num_time_series, series_length)
        x = x.view(batch_size, -1, self.series_length)
        # x = x.view(batch_size, 1, -1)


        # print("NAN A", x.isnan().any())  # print to find source of NaN values!

        # Forward pass through convolutional layers
        x = self._forward_conv_layers(x)
        # print("conv_layers: ", x.shape)
        # print("NAN B", x.isnan().any())

        # Flatten the output
        x = x.view(batch_size, -1)
        # print("flatten: ", x.shape)

        # Forward pass through fully connected layers
        x = self.fc_layers(x)
        # print("NAN C", x.isnan().any())
        # print("fc_layers: ", x.shape)

        # x = torch.softmax(x, dim=1)  # get probabilities for each class
        # add log softmax
        # x = torch.log_softmax(x, dim=1)
        return x

