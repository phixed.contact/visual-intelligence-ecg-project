import torch
import torch.nn as nn


class MultiTimeSeriesToBinary_v2(nn.Module):
    def __init__(self, num_time_series, series_shape, batch_size, num_classes=2):
        super(MultiTimeSeriesToBinary_v2, self).__init__()

        self.series_shape = series_shape
        self.batch_size = batch_size

        self.batch_norm = nn.BatchNorm1d(num_time_series)

        # Define the convolutional layers
        self.conv1d_layers = nn.ModuleList([
            nn.Conv1d(in_channels=num_time_series, out_channels=16, kernel_size=5, padding=2),
            nn.Conv1d(in_channels=16, out_channels=32, kernel_size=5, padding=2),
            # nn.Conv1d(in_channels=24, out_channels=24, kernel_size=3),
            # nn.Conv1d(in_channels=64, out_channels=64, kernel_size=3),
        ])

        # Calculate the input size for the fully connected layer
        conv_output_size = self._get_conv_output_size(num_time_series, series_shape)

        print("aa conv_output_size: ", conv_output_size)

        # Define the fully connected layers
        self.fc_layers = nn.Sequential(
            nn.Linear(conv_output_size, 128),
            # nn.Linear(series_shape, 256),
            nn.ReLU(),
            nn.BatchNorm1d(128),
            nn.Dropout(0.2),  # TODO: should I use dropout?
            nn.Linear(128, 64),
            nn.ReLU(),
            # nn.BatchNorm1d(128),
            # nn.Dropout(0.2),
            nn.Linear(64, num_classes),
            # no real difference if using softmax or logsoftmax or none, kinda strange
            # nn.Softmax(dim=1)
            # nn.LogSoftmax(dim=1)
        )

        self.softmax = nn.LogSoftmax(dim=1)

        # Weights initialization
        for layer in self.fc_layers:
            if isinstance(layer, nn.Linear):
                nn.init.xavier_uniform_(layer.weight)

    def _get_conv_output_size(self, num_time_series, series_shape):
        print("_get_conv_output_size")
        # Dummy input to calculate the size after convolutional layers
        x = torch.randn(1, num_time_series, series_shape)  # series_shape[0] * series_shape[1])
        print(x.shape)
        x = self._forward_conv_layers(x)
        print(x.view(1, -1).shape)
        print(x.view(1, -1).size(1))
        return x.view(1, -1).size(1)

    def _forward_conv_layers(self, x):
        for conv_layer in self.conv1d_layers:
            x = conv_layer(x)
            x = nn.ReLU()(x)
            x = nn.MaxPool1d(kernel_size=2, stride=2)(x)
            # x = nn.AvgPool1d(3)(x)
            # x = nn.BatchNorm1d(x.shape[2])
        return x

    def forward(self, x):
        # print("forward: ", x.shape)
        # Reshape input to (batch_size, num_time_series * series_length)
        batch_size = x.size(0)
        # print("forward x: ", x)
        # Reshape to (batch_size, num_time_series, series_length)
        # x = x.view(batch_size, -1, self.series_shape[0] * self.series_shape[1])
        # x = x.view(batch_size, 1, -1)

        # add batch normalization
        x = self.batch_norm(x)


        # print("NAN A", x.isnan().any())  # print to find source of NaN values!

        # Forward pass through convolutional layers

        x = self._forward_conv_layers(x)
        # print("after conv: ", x)
        # print("conv_layers: ", x.shape)
        # print("NAN B", x.isnan().any())

        # Flatten the output
        x = x.view(batch_size, -1)
        # print("flatten: ", x.shape)

        # Forward pass through fully connected layers
        x = self.fc_layers(x)
        # print("NAN C", x.isnan().any())
        # print("fc_layers: ", x.shape)

        # x = torch.softmax(x, dim=1)  # get probabilities for each class
        # add log softmax
        # x = torch.log_softmax(x, dim=1)
        # x = torch.softmax(x, dim=1)
        # sigmoid activation
        # x = torch.sigmoid(x)
        x = self.softmax(x)
        return x

