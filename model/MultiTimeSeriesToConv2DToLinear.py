import torch
import torch.nn as nn


class MultiTimeSeriesToConv2DToLinear(nn.Module):
    def __init__(self, num_time_series, series_shape, batch_size, num_classes=2):
        super(MultiTimeSeriesToConv2DToLinear, self).__init__()

        self.series_shape = series_shape
        self.batch_size = batch_size
        self.num_time_series = num_time_series

        self.conv1 = nn.Sequential(
            nn.Conv2d(num_time_series, 64, kernel_size=(3,3)),
            nn.BatchNorm2d(64),
            # nn.Sigmoid()
            nn.ReLU(),
            )
        self.conv2 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=(3,3)),
            nn.BatchNorm2d(64),
            # nn.Sigmoid(),
            nn.ReLU(),
            nn.MaxPool2d((3,3)),
            # nn.AvgPool1d(3, stride=2),
            # nn.Dropout(0.1)
            )
        self.conv3 = nn.Sequential(
            nn.Conv2d(64, 12, kernel_size=(3,3)),
            nn.BatchNorm2d(12),
            # nn.Sigmoid(),
            nn.ReLU(),
            nn.MaxPool2d((3,3)),
            # nn.AvgPool1d(3, stride=2),
            # nn.Dropout(0.1)
        )
        self.fc_1 = nn.Sequential(
            nn.Linear(288, 128),
            # nn.BatchNorm1d(256),
            nn.ReLU(),
            # nn.Dropout(0.3),
            # nn.Linear(256, 128),
            # nn.ReLU(),
            # nn.Dropout(0.1),
            nn.Linear(128, num_classes),
            # nn.LogSoftmax(dim=1)
            nn.Softmax(dim=1)
        )

        self.flatten_layer = nn.Flatten()

        # Weights initialization
        for layer in self.fc_1:
            if isinstance(layer, nn.Linear):
                nn.init.xavier_uniform_(layer.weight)

    def forward(self, x):
        batch_size = x.size(0)
        # x = x.view(batch_size, self.series_shape[0], self.series_shape[1])
        # print("forward: ", x.shape)
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.flatten_layer(x)
        x = self.fc_1(x)
        return x

