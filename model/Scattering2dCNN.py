import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim
from kymatio.torch import Scattering2D


class Scattering2dCNN(nn.Module):
    def __init__(self, num_classes, scattering_params: dict):
        super(Scattering2dCNN, self).__init__()
        self.scattering = Scattering2D(**scattering_params)
        self.conv1 = nn.Conv2d(217, 32, kernel_size=(3,1), stride=1)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=(3,1), stride=1)
        self.conv3 = nn.Conv2d(64, 128, kernel_size=(3,1), stride=1)
        self.fc1 = nn.Linear(128, 128)
        self.fc2 = nn.Linear(128, num_classes)

    def forward(self, x):
        x = self.scattering(x)
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2)
        x = F.relu(self.conv3(x))
        x = torch.flatten(x, 1)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x