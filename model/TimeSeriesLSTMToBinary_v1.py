import torch
import torch.nn as nn


class TimeSeriesLSTMToBinary_v1(nn.Module):
    def __init__(self, num_time_series, series_shape, batch_size, num_classes=2):
        super(TimeSeriesLSTMToBinary_v1, self).__init__()

        self.series_shape = series_shape
        self.batch_size = batch_size
        self.num_time_series = num_time_series

        self.hidden_size = 128
        input_size = series_shape[1]
        self.num_layers = 3

        self.lstm = nn.LSTM(input_size, self.hidden_size, self.num_layers, batch_first=True)
        self.fc_1 = nn.Linear(self.hidden_size, 64)
        self.fc_2 = nn.Linear(64, num_classes)

        self.relu = nn.ReLU()

        self.batch_norm = nn.BatchNorm1d(series_shape[0])

        # # Weights initialization
        # for layer in self.multi_layer_perceptron:
        #     if isinstance(layer, nn.Linear):
        #         nn.init.xavier_uniform_(layer.weight)

    def forward(self, x):
        x = x.view(-1, self.series_shape[0], self.series_shape[1])
        # Initialize hidden state with zeros
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).double().to(x.device)
        # Initialize cell state with zeros
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).double().to(x.device)

        x = self.batch_norm(x)
        out, _ = self.lstm(x, (h0, c0))
        out = self.relu(out)
        out = self.fc_1(out[:, -1, :])
        out = self.relu(out)
        out = self.fc_2(out)
        out = torch.nn.functional.log_softmax(out, dim=1)
        return out

