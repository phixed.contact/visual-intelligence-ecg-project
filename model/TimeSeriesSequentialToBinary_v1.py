import torch
import torch.nn as nn


class TimeSeriesSequentialToBinary_v1(nn.Module):
    def __init__(self, num_time_series, series_length, batch_size, num_classes=2):
        super(TimeSeriesSequentialToBinary_v1, self).__init__()

        self.series_length = series_length
        self.batch_size = batch_size
        self.num_time_series = num_time_series

        self.multi_layer_perceptron = nn.Sequential(
            # nn.BatchNorm1d(233),
            nn.Linear(233, 128),
            # nn.BatchNorm1d(128),
            nn.ReLU(),
            # nn.Sigmoid(),
            # nn.Dropout(0.3),
            nn.Linear(128, 128),
            nn.ReLU(),
            # nn.Sigmoid(),
            nn.Linear(128, 64),
            nn.ReLU(),
            # nn.Sigmoid(),
            # nn.Dropout(0.1),
            nn.Linear(64, num_classes),
            nn.LogSoftmax(dim=1)
            # nn.Softmax(dim=1)
            # nn.Sigmoid()
        )

        self.flatten_layer = nn.Flatten()

        # Weights initialization
        for layer in self.multi_layer_perceptron:
            if isinstance(layer, nn.Linear):
                nn.init.xavier_uniform_(layer.weight)

    def forward(self, x):
        x = self.flatten_layer(x)
        x = self.multi_layer_perceptron(x)
        return x

