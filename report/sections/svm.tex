% !TeX spellcheck = en_GB

\subsection{Binary classification}
For a first classification of the \gls{ECG} signals, a \gls{SVM} is used. The advantage of \gls{SVM}s are there very good ability of dealing with a lot of features and with non-distinctive features. The actual implementation can be found in the file \textbf{scattering\_svm.ipynb}.\\

A \gls{SVM} is applied on each channel of the mean of the scattering result from section \ref{sec:scattering}. 
Applying the mean results in the scattering result of shape $(233, 19)$ being reduced to a feature vector with length $233$.
Figure \ref{img:arrhytmia_nonarrhytmia} shows the scattering mean sample of arrhytmia and non-arrhytmia. Here a possible first distinction between the two classes can be seen.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/arrhytmia_nonarrhytmia.png}
	\caption{Scattering mean sample of arrhytmia and non-arrhytmia, channel V4}
	\label{img:arrhytmia_nonarrhytmia}
\end{figure}

By using \gls{RFE}, the most important coefficients can be extracted and displayed, see figure \ref{img:rfe_binary_three_features}.
Based on the separation of the two classes it can already be seen that the coefficients are not very distinctive, indicating that a classification using only the scattering coefficients might not be very accurate.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/rfe_binary_three_features.png}
	\caption{Three most distinctive features for binary classification based on RFE, channel V4}
	\label{img:rfe_binary_three_features}
\end{figure}

For increasing the discrimination between the features, \gls{PCA} is applied. 
The idea of PCA is to reduce the dimension of the feature space. The scattering coefficients are reduced to 3 dimensions and the plot of the scattering coefficients is shown in figure \ref{img:pca_binary_plot}.
Here the separation of the data is still not very distinctive.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/pca_binary_plot.png}
	\caption{Applying PCA to reduce feature size to 3, channel V4}
	\label{img:pca_binary_plot}
\end{figure}

Apart from the not very distinctive features on a reduced feature space, a \gls{SVM} is used for classifying the binary labels on each channel.
For this a random subsample of 5000 samples is taken from the whole dataset, with 3974 samples belonging to the non-arrhytmia and 1026 to the arrytmia class. 
80\% are used for fitting the \gls{SVM} and the remaining 20\% are used for evaluation.
The results for each channel are shown in table \ref{tab:binary_svm_results}. The last column \textit{Combined} is the accuracy by combining the prediction of all channels.

\begin{table}[H]
	\caption{Binary SVM results by channel}
	\label{tab:binary_svm_results}
	\begin{center}
	\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}
	\hline
	Channel  & I     & II   & III   & aVR   & aVL   & aVF   & V1    & V2    & V3    & V4    & V5    & V6    & Combined \\ \hline
	Accuracy & 0.944 & 0.94 & 0.922 & 0.945 & 0.928 & 0.932 & 0.943 & 0.938 & 0.934 & 0.936 & 0.942 & 0.934 & 0.95     \\ \hline
	\end{tabular}
	\end{center}
\end{table}

Table \ref{tab:binary_svm_conf_matrix} shows the confusion matrix for the combined prediction.

\begin{table}[]
	\caption{Confusion matrix combined channels binary SVM}
	\label{tab:binary_svm_conf_matrix}
	\begin{center}
	\begin{tabular}{|l|c|c|}
	\hline
				  & \multicolumn{1}{l|}{Non-Arrhytmia}                   & \multicolumn{1}{l|}{Arrhytmia}                       \\ \hline
	Non-Arrhytmia & \begin{tabular}[c]{@{}c@{}}784\\ (0.96)\end{tabular} & \begin{tabular}[c]{@{}c@{}}17\\ (0.09)\end{tabular}  \\ \hline
	Arrhytmia     & \begin{tabular}[c]{@{}c@{}}33\\ (0.04)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}166\\ (0.91)\end{tabular} \\ \hline
	\end{tabular}
\end{center}
	\end{table}

\subsection{Multi class classification}
In the second classification the four classes from table \ref{tab:classesandconditions} are classified, with the same procedure as for the binary classification. For the implementation see file \textbf{scattering\_svm\_multiclass.ipynb}.
The class distribution is: 
\begin{itemize}
	\item AFIB: 1914 samples
	\item GSVT: 114 samples
	\item SB: 1914 samples
	\item SR: 1956 samples
\end{itemize}

The results are shown in table \ref{tab:binary_svm_results}. The last column \textit{Combined} is the accuracy by combining the prediction of all channels.
\begin{table}[H]
	\caption{Multi class SVM results by channel}
	\label{tab:binary_svm_results}
	\begin{center}
	\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}
	\hline
	Channel  & I     & II   & III   & aVR   & aVL   & aVF   & V1    & V2    & V3    & V4    & V5    & V6    & Combined \\ \hline
	Accuracy & 0.916 & 0.932 & 0.913 & 0.927 & 0.904 & 0.916 & 0.925 & 0.925 & 0.922 & 0.918 & 0.927 & 0.918 & 0.931     \\ \hline
	\end{tabular}
	\end{center}
\end{table}

For the \textbf{Combined} prediction the confusion matrix is shown in table \ref{tab:confmatrixmulticlasssvm}.
\begin{table}[H]
	\caption{Confusion matrix combined channels multi class SVM}
	\label{tab:confmatrixmulticlasssvm}
	\begin{center}
	\begin{tabular}{l|c|c|c|c}
	\multicolumn{1}{l|}{}     & \multicolumn{1}{l|}{AFIB}                                                  & \multicolumn{1}{l|}{GSVT}                                                & \multicolumn{1}{l|}{SB}                                                   & \multicolumn{1}{l}{SR}                                                  \\ \hline
	\multicolumn{1}{l|}{AFIB} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}171\\ (0.855)\end{tabular}} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}1\\ (0.083)\end{tabular}} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}18\\ (0.043)\end{tabular}} & \multicolumn{1}{c}{\begin{tabular}[c]{@{}c@{}}7\\ (0.019)\end{tabular}} \\ \hline
	GSVT                       & \begin{tabular}[c]{@{}c@{}}10\\ (0.05)\end{tabular}                        & \begin{tabular}[c]{@{}c@{}}9\\ (0.75)\end{tabular}                       & \begin{tabular}[c]{@{}c@{}}0\\ (0.0)\end{tabular}                         & \begin{tabular}[c]{@{}c@{}}4\\ (0.01)\end{tabular}                       \\ \hline
	SB                         & \begin{tabular}[c]{@{}c@{}}5\\ (0.025)\end{tabular}                        & \begin{tabular}[c]{@{}c@{}}0\\ (0.0)\end{tabular}                        & \begin{tabular}[c]{@{}c@{}}389\\ (0.94)\end{tabular}                      & \begin{tabular}[c]{@{}c@{}}1\\ (0.003)\end{tabular}                      \\ \hline
	SR                         & \begin{tabular}[c]{@{}c@{}}14\\ (0.07)\end{tabular}                        & \begin{tabular}[c]{@{}c@{}}2\\ (0.17)\end{tabular}                       & \begin{tabular}[c]{@{}c@{}}7\\ (0.017)\end{tabular}                       & \begin{tabular}[c]{@{}c@{}}362\\ (0.97)\end{tabular}                    
	\end{tabular}
\end{center}
\end{table}


\subsection{Evaluation}
Applying scattering transform and using a \gls{SVM} for classification showed to be a very good approach, with accuracies of \SI{95}{\%} for detecting Arrhytmia and \SI{93}{\%} for the multi class classification.
Based on those results, a further analysis using scattering transform with neuronal network will be performed in the following section.



