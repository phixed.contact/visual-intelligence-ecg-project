import random
import glob
import wfdb
import numpy as np
from IPython.display import clear_output


def load_number_of_random_samples(ecg_data_path, number_of_samples, seed=1905):
    """
    Loads a number of random samples from the supplied path.

    :param path: The path to the files.
    :param number_of_samples: The number of samples to load. Float for percentage or int for number of samples or -1 for all samples.
    :return: A list of samples.
    """
    all_samples = glob.glob(ecg_data_path + '*/*/*.mat')
    random.seed(seed)
    if 1 > number_of_samples > 0:
        number_of_samples = int(number_of_samples * len(all_samples))
    elif number_of_samples > len(all_samples):
        number_of_samples = len(all_samples)
    elif number_of_samples == -1:
        number_of_samples = len(all_samples)
    elif number_of_samples == 0:
        return []

    random_samples = random.choices(all_samples, k=len(all_samples))
    result = []
    error_count = 0
    count = 0
    for sample in random_samples:
        if count % 100 == 0:
            clear_output(wait=True)
            print(f"Loaded {count} of {number_of_samples}")
        if count >= number_of_samples:
            break
        try:
            record = wfdb.io.rdsamp(sample.replace(".mat", ""))
            labels = get_label_identifiers_from_record(record)
            if labels is None or len(labels) == 0:
                print("invalid entry")
                continue
            multi_label = turn_label_into_multi_label(labels, get_multi_class_identifiers())
            binary_label = turn_label_into_binary_healthy_unhealty(labels, get_arrhytmia_identifiers())
            if multi_label is None or binary_label is None:
                print(labels)
                if multi_label is None and binary_label is None:
                    print("excluded label...")
                else:
                    print("no valid multi label...")
                continue
            result.append({
                'data': np.array(record[0]),
                'labels': labels,
                'binary_label': binary_label,
                'multi_label': multi_label
            })
            count += 1
        except:
            error_count += 1
            continue
    if error_count > 0:
        print(f"load_number_of_random_samples error count: {error_count}")
    return result


def load_number_of_random_samples_balanced(ecg_data_path, number_of_samples, seed=1905):
    """
    Loads a number of random samples from the supplied path.

    :param path: The path to the files.
    :param number_of_samples: The number of samples to load. Float for percentage or int for number of samples or -1 for all samples.
    :return: A list of samples.
    """
    all_samples = glob.glob(ecg_data_path + '*/*/*.mat')
    random.seed(seed)
    random_samples = random.choices(all_samples, k=len(all_samples))
    if 1 > number_of_samples > 0:
        number_of_samples = int(number_of_samples * len(all_samples))
    elif number_of_samples > len(all_samples):
        number_of_samples = len(all_samples)
    elif number_of_samples == -1:
        number_of_samples = len(all_samples)
    elif number_of_samples == 0:
        return []
    result = []
    error_count = 0
    count = 0
    count_healthy = 0
    count_unhealthy = 0
    for sample in random_samples:
        if count % 25 == 0:
            clear_output(wait=True)
            print(f"Loaded {count} of {number_of_samples}")
        if count >= number_of_samples:
            break
        try:
            record = wfdb.io.rdsamp(sample.replace(".mat", ""))
            labels = get_label_identifiers_from_record(record)
            if labels is None or len(labels) == 0:
                continue
            temp_binary_label = turn_label_into_binary_healthy_unhealty(labels, get_arrhytmia_identifiers())
            if temp_binary_label == 0 and count_healthy >= (number_of_samples / 2):
                continue
            elif temp_binary_label == 1 and count_unhealthy >= (number_of_samples / 2):
                continue
            if temp_binary_label is None:
                continue
            if temp_binary_label == 0:
                count_healthy += 1
            else:
                count_unhealthy += 1
            count += 1
            result.append({
                'data': np.array(record[0]),
                'labels': labels,
                'binary_label': temp_binary_label})
        except:
            error_count += 1
            continue
    if error_count > 0:
        print(f"load_number_of_random_samples error count: {error_count}")
    return result

def get_count_all_labels(ecg_data_path, limit_samples=None):
    all_samples = glob.glob(ecg_data_path + '*/*/*.mat')
    result = {}
    error_count = 0
    count = 0
    for sample in all_samples:
        count += 1
        if count % 100 == 0:
            clear_output(wait=True)
            print(f"Processing {count} of {len(all_samples)}")
        if limit_samples is not None and count > limit_samples:
            break
        try:
            # Note: rdheader is probably more efficient, but does not contain the labels for some reason...
            record = wfdb.io.rdsamp(sample.replace(".mat", ""))
            labels = get_label_identifiers_from_record(record)
            if labels is None or len(labels) == 0:
                continue
            for label in labels:
                if label not in result:
                    result[label] = 1
                else:
                    result[label] += 1
        except:
            error_count += 1
            continue
    if error_count > 0:
        print(f"get_count_all_labels error count: {error_count}")
    return result


def get_label_identifiers_from_record(record):
    """
    Gets the label identifier from a wfdb record.

    :param record: The record.
    :return: The label identifier.
    """
    comment_array = record[1]['comments']
    dx_entry = [i for i in comment_array if i.startswith('Dx:')]
    if dx_entry is None or not len(dx_entry):
        print("invalid entry")
        return None
    return dx_entry[0].split(':')[1].strip().split(',')


def labels_to_exclude():
    # According to the exclusion criteria in Section 2.1, all records without a code or with at least one of the following codes were excluded: “365413008” (poor R wave progression), “251146004” (low QRS voltages), or “10370003” (pacing rhythm).
    # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9413391/
    return ['365413008', '251146004', '10370003']

def get_multi_class_identifiers():
    # possible sub classes, based upon optimal "Multi-Stage Arrhythmia  Classification Approach"
    ### AFIB
    # atrial fibrillation 164889003  -> the common type of arrhythmia, atrial fibrillation (AFIB)
    # AF,Atrial Flutter,164890007 -> another type of arrhythmia, atrial flutter (AFL)

    ### GSVT - General supraventricular tachycardia
    # supraventricular tachycardia 426761007
    # atrial tachycardia 713422000
    # atrioventricular node reentrant tachycardia 233896004
    # atrioventricular reentrant tachycardia 233897008

    ### SB - Sinus Bradycardia
    # sinus bradycardia 426177001 - cardiac rhythm with appropriate cardiac muscular depolarization initiating from the sinus node and a rate of fewer than 60 beats per minute

    ### SR - Sinus Rhytm
    # sinus rhytm 426783006
    # SA,Sinus Irregularity,427393009
    # Sinus Tachycardia,427084000

    multi_class = [
        {
            'name': 'AFIB',
            'label': 0,
            'identifier': ['164889003', '164890007']
        },
        {
            'name': 'GSVT',
            'label': 1,
            'identifier': ['426761007', '713422000', '233896004', '233897008']
        },
        {
            'name': 'SB',
            'label': 2,
            'identifier': ['426177001']
        },
        {
            'name': 'SR',
            'label': 3,
            'identifier': ['426783006', '427084000', '427393009']
        }
    ]
    return multi_class


def turn_label_into_multi_label(labels, multi_class_identifiers):
    # check if excluded
    for label in labels:
        if label in labels_to_exclude():
            return None
    for label in labels:
        for multi_class in multi_class_identifiers:
            if label in multi_class['identifier']:
                return multi_class['label']
    return None


def get_arrhytmia_identifiers():
    ### AFIB
    # atrial fibrillation 164889003  -> the common type of arrhythmia, atrial fibrillation (AFIB)
    # AF,Atrial Flutter,164890007 -> another type of arrhythmia, atrial flutter (AFL)
    return ['164889003', '164890007']


def turn_label_into_binary_healthy_unhealty(labels, unhealty_identifiers):
    """
    Turns the label into a binary label healthy/unhealthy based on the supplied identifiers.

    :param unhealty_identifiers: list of identifiers which are considered unhealthy.
    :param labels: The labels. 0 healthy, 1 unhealthy.
    :return: The binary label.
    """
    # check if excluded
    for label in labels:
        if label in labels_to_exclude():
            return None
    return max([1 if identifier in labels else 0 for identifier in unhealty_identifiers])

