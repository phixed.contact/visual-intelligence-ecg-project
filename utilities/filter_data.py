from scipy.signal import butter, filtfilt, iirnotch, kaiserord, firwin
import pandas as pd
import numpy as np
from loess.loess_1d import loess_1d  # https://pypi.org/project/loess/  # TODO: package needs to be cited if used
import pywt


def apply_low_pass_filter(signal, cutoff_hz, sample_frequency=500, order=3):
    nyguist = 0.5 * sample_frequency
    cutoff = cutoff_hz / nyguist
    b, a = butter(order, cutoff, btype='low', output='ba')
    filtered_signal = filtfilt(b, a, signal)
    return filtered_signal


def apply_notch_filter(signal, powerline_freq=50, sample_frequency=500, quality_factor=30):
    notch_freq = powerline_freq / (sample_frequency / 2)
    b, a = iirnotch(notch_freq, quality_factor)
    return filtfilt(b, a, signal)


def add_noise_to_signal(signal, noise_amplitude=0.05):
    noise = np.random.normal(0, noise_amplitude, len(signal))
    return signal + noise

def apply_adaptive_filter(signal, filter_length=32, step_size=0.05):
    # LMS adaptive filter
    # TODO: step_size is an important value, should be optimized...
    # w = np.zeros(filter_length)
    # clean_signal = np.zeros_like(signal)
    # for n in range(filter_length, len(signal)):
    #     x = signal[n - filter_length:n]
    #     y = np.dot(w, x)
    #     e = signal[n] - y
    #     w += step_size * e * x
    #     clean_signal[n] = y
    # return clean_signal


    filtered_ecg = np.zeros_like(signal)
    filter_coefficients = np.zeros(filter_length)

    for i in range(filter_length, len(signal)):
        # Extract a window of the past filter_length samples
        x = signal[i - filter_length:i]

        # Calculate the desired signal (error)
        d = signal[i]

        # Calculate the output of the filter
        y = np.dot(filter_coefficients, x)

        # Update the filter coefficients
        error = d - y
        filter_coefficients = filter_coefficients + step_size * error * x

        # Store the filtered signal
        filtered_ecg[i] = d - y

    return filtered_ecg



def apply_loess_filter(signal, degree=1, frac=0.5):
    xout, yout, wout = loess_1d(np.arange(0, len(signal), 1), signal, xnew=None, degree=degree, frac=frac,
                                      npoints=None, rotate=False, sigy=None)
    return pd.Series(yout, index=xout)


def apply_dwt_to_eliminate_baseline_wander(signal, wavelet='sym8', level=8, hard_threshold=False):
    # hard_threshold False resulted in some infinity values...
    # https://biomed.bas.bg/bioautomation/2021/vol_25.2/files/25.2_06.pdf
    # DWT
    coeffs = pywt.wavedec(signal, wavelet, level=level)

    # hard thresholding using sure
    if hard_threshold:
        # Estimate noise variance using median absolute deviation (MAD)
        noise_est = np.median(np.abs(coeffs[-1] - np.median(coeffs[-1]))) / 0.6745
        # Calculate universal threshold using SURE
        threshold = np.sqrt(2 * np.log(len(signal))) * noise_est
        denoised_coeffs = [pywt.threshold(c, threshold, mode='hard') for c in coeffs]
    else:
        # soft thresholding
        threshold = 0.1 * np.max(np.abs(coeffs[0]))
        denoised_coeffs = [pywt.threshold(c, threshold, mode='soft') for c in coeffs]

    # reconstruct signal
    denoised_signal = pywt.waverec(denoised_coeffs, wavelet)
    # TODO: somehow fix this, weird behavior, but only sometimes...
    if np.isnan(denoised_signal).any():
        print("DWT denoising failed")
        return signal
    return denoised_signal


def fir_filter_to_eliminate_baseline_wander(signal, sample_frequency=500, cutoff_frequency=0.67, factor=2):
    # Note: pretty slow...
    # based on paper: https://paperswithcode.com/paper/180711359  -> supposed to be good baseline removal filter
    # https://github.com/fperdigon/DeepFilter/blob/master/digitalFilters/dfilters.py
    #    ecgy:        the contamined signal (must be a list)
    #    Fc:          cut-off frequency
    #    Fs:          sample frequiency
    #    ECG_Clean :  processed signal without BLW

    # getting the length of the signal
    signal_len = len(signal)

    # The Nyquist rate of the signal.
    nyq_rate = sample_frequency / 2.0

    # The desired width of the transition from stop to pass,
    # relative to the Nyquist rate.
    width = 0.07 / nyq_rate

    # Attenuation in the stop band, in dB.
    # related to devs in Matlab. On Matlab is on proportion
    ripple_db = round(-20 * np.log10(0.001)) + 1
    ripple_db = ripple_db / factor

    # Compute the order and Kaiser parameter for the FIR filter.
    N, beta = kaiserord(ripple_db, width)

    # Use firwin with a Kaiser window to create a highpass FIR filter.
    N |= 1  # Ensure N is odd -> https://stackoverflow.com/questions/57633842/designing-band-stop-filter-at-scipy-firwin
    h = firwin(N, cutoff_frequency / nyq_rate, window=('kaiser', beta), pass_zero='highpass')

    # Check filtfilt condition
    if N * 3 > signal_len:
        diff = N * 3 - signal_len
        ecgy = list(reversed(signal)) + list(signal) + list(signal[-1] * np.ones(diff))

        # Filtering with filtfilt
        ECG_Clean = filtfilt(h, 1.0, ecgy)
        ECG_Clean = ECG_Clean[signal_len: signal_len + signal_len]
    else:
        ECG_Clean = filtfilt(h, 1.0, signal)

    return ECG_Clean
